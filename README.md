# 控制器参数生成器 Laravel 适配器

## 介绍
通过注解的形式直接生成控制器参数

## 安装
```shell
composer require dreamcat/object-orm-4-laravel
```

## 使用方法
### 加入 provider
- `\DreamCat\ObjectOrm4Laravel\OrmProvider`

### 配置定义
配置文件放在 `config/orm.php`

 配置路径 | 类型 | 含义 | 默认值
--- | --- | --- | ---
disableCamel | bool | 是否启用自动将数据库的下划线风格改为驼峰风格 | false

