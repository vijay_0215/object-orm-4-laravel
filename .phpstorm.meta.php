<?php

namespace PHPSTORM_META {

    use DreamCat\ObjectOrm4Laravel\Orm;
    use Illuminate\Contracts\Container\Container;

    override(Orm::toModel(0, 1), type(1));
    override(Orm::toModel(0, 1), map(["" => "$1[]"]));

    override(Container::make(0), type(0));
}

# end of file
