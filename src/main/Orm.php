<?php

declare(strict_types=1);

namespace DreamCat\ObjectOrm4Laravel;

use DreamCat\ObjectOrm\OrmInterface;
use Illuminate\Support\Collection;
use stdClass;

/**
 * Orm快捷操作方法
 * @author vijay
 */
class Orm
{
    /**
     * 将模型对象转为要存入数据库的数据
     * @param object|object[] $modelObj 数据模型或模型数组
     * @return array 要存入的数据
     */
    public static function toDatabaseArray(object|array $modelObj): array
    {
        /** @var OrmInterface $orm */
        $orm = app(OrmInterface::class);
        if (is_array($modelObj)) {
            return array_map(
                [
                    $orm,
                    "toDatabaseArray",
                ],
                $modelObj
            );
        } else {
            return $orm->toDatabaseArray($modelObj);
        }
    }

    /**
     * 将数据库检索出的数据转换为类
     * @param Collection|stdClass[]|stdClass|null $model 检索出的数据或数据数组
     * @param string $modelType 数据类名
     * @return object|object[]|null 模型对象或模型对象数组
     */
    public static function toModel(Collection|array|stdClass|null $model, string $modelType): object|array|null
    {
        if (is_null($model)) {
            return null;
        }
        /** @var OrmInterface $orm */
        $orm = app(OrmInterface::class);
        if (is_a($model, Collection::class)) {
            $list = [];
            foreach ($model as $value) {
                $list[] = self::toModel($value, $modelType);
            }
            return $list;
        } elseif (is_array($model)) {
            return array_map(
                function ($value) use ($modelType) {
                    return self::toModel($value, $modelType);
                },
                $model
            );
        } else {
            return $orm->toModel($model, $modelType);
        }
    }
}

# end of file
