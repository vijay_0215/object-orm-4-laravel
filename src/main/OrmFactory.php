<?php

declare(strict_types=1);

namespace DreamCat\ObjectOrm4Laravel;

use DreamCat\Array2Class\TransImpl\CamelCase2UnderLine;
use DreamCat\ObjectOrm\Array2Class;
use DreamCat\ObjectOrm\Class2Array;
use DreamCat\ObjectOrm\ObjectOrm;
use DreamCat\ObjectOrm\OrmInterface;
use DreamCat\Psr4Laravel\Bean\PsrContainer;

/**
 * Orm工厂类
 * @author vijay
 */
class OrmFactory
{
    /**
     * 创建orm转换器对象
     * @return OrmInterface orm转换器
     */
    public function create(): OrmInterface
    {
        $container = new PsrContainer(app());
        $orm = new ObjectOrm($container);
        $config = config("orm");
        $array2class = new Array2Class($container);
        $fixer = new Class2Array($container);
        $fixer->setIgnoreUninit(true);
        if (!isset($config["disableCamel"]) || !$config["disableCamel"]) {
            $array2class->setPropertyNameTrans(new CamelCase2UnderLine());
            $fixer->setPropertyNameTrans(new CamelCase2UnderLine());
        }
        $orm->setArray2Class($array2class);
        $orm->setJsonValueFixer($fixer);
        return $orm;
    }
}

# end of file
