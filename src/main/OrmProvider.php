<?php

declare(strict_types=1);

namespace DreamCat\ObjectOrm4Laravel;

use DreamCat\ObjectOrm\OrmInterface;
use Illuminate\Support\ServiceProvider;

/**
 * orm对象的provider
 * @author vijay
 */
class OrmProvider extends ServiceProvider
{
    /**
     * @inheritDoc
     * @noinspection PhpMissingParentCallCommonInspection
     */
    public function register(): void
    {
        $this->app->singleton(OrmInterface::class, function () {
            return $this->app
                ->make(OrmFactory::class)
                ->create();
        });
    }
}

# end of file
